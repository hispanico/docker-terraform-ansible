# Terrafoem Ansible Test Docker Image

| **Master** | **Develop** |
| :---: | :---: |
| [![pipeline status](https://gitlab.com/hispanico/docker-terraform-ansible/badges/master/pipeline.svg)](https://gitlab.com/hispanico/docker-terraform-ansible/commits/master) | [![pipeline status](https://gitlab.com/hispanico/docker-terraform-ansible/badges/develop/pipeline.svg)](https://gitlab.com/hispanico/docker-terraform-ansible/commits/develop)|

Terrafomr Docker containers for Ansible playbook and role testing.

## How to Use

TBD
